# Biblio eXtemporanea

Percorsi bibliografici comuni del collettivo eXtemporanea.

## Come partecipare?

Il modo più semplice di partecipare alla Biblioteca è di unirsi al gruppo su Zotero

https://www.zotero.org/groups/2574142/extemporanea/library

Si accede al gruppo in due passi:
1. creazione di un account su Zotero https://www.zotero.org/user/register
2. ricerca e aggiunta della libreria condivisa (*shared library*) Scienceground. Per poter modificare i contenuti, bisogna richiere l'accesso (e attendere la risposta). Per più informazioni sui gruppi in Zotero, vedere qui https://www.zotero.org/support/groups

Il modo più comodo di interagire con la libreria è di utilizzare il client. Questo può essere scaricato da qui https://www.zotero.org/download/
